**Simple utility which converts among name convention casings**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Supported cases

plain
snake
caterpillar 
path
dot
sentence
constant
train
title
header
pascal
camel

---

## How to use the shell binary?
  The shell binary is located in the root of the repository. called "caser.exe".
### The format:
```
caser.exe --from [CASE_NAME] --to [CASE_NAME] [text to process] 
```
  it can also take the standard input to process if no text is given as an argument
