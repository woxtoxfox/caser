import 'dart:io';

import 'package:args/args.dart';

// Utils
String makeFirstCharCaps(String x) {
  return x.replaceFirstMapped(RegExp('[a-z]'), (match) {
    return x[match.start].toUpperCase();
  });
}

String invertFirstCharCapitalization(String x) {
  return invertCapitalization(x.substring(0, 1)) + x.substring(1);
}

String invertCapitalization(String x) {
  return String.fromCharCodes(x.codeUnits.map((int e) {
    if (e > 64 && e < 91) return e + 32;
    if (e > 96 && e < 123) return e - 32;
    return e;
  }));
}

List<String> splitStringByIndexes(String x, Iterable<int> indexes) {
  List<String> splited = [];
  StringBuffer elementBuffer = StringBuffer();
  for (int i = 0; i < x.length; i++) {
    if (indexes.contains(i)) {
      splited.add(elementBuffer.toString());
      elementBuffer.clear();
    }
    elementBuffer.write(x[i]);
  }
  splited.add(elementBuffer.toString());
  return splited;
}

// Case
class Case {
  RegExp splitPattern;
  bool doSplitCharSwallow;
  var accumulator;
  Case(this.splitPattern, this.doSplitCharSwallow, this.accumulator);
  Case.stitched(
    String stitchChar, {
    bool doCapitalize = false,
    bool doCapitalizeFirstCharOfWords = false,
    bool doInvertFirstCharCapitalization = false,
    RegExp? manualSplitPattern,
  })  : doSplitCharSwallow = true,
        splitPattern = manualSplitPattern ?? RegExp(stitchChar) {
    accumulator = (String total, String word) {
      word = doCapitalize ? word.toUpperCase() : word.toLowerCase();
      word = doCapitalizeFirstCharOfWords ? makeFirstCharCaps(word) : word;
      if (total.isEmpty) {
        return doInvertFirstCharCapitalization
            ? invertFirstCharCapitalization(word)
            : word;
      }
      if (word.isEmpty) return total;
      return total + stitchChar + word;
    };
  }
  String encode(List<String> words) {
    words.insert(0, '');
    return words.reduce(accumulator);
  }

  List<String> decode(String text) {
    int startIndents = text.codeUnits.length - text.trimLeft().codeUnits.length;
    int endIndents = text.codeUnits.length - text.trimRight().codeUnits.length;
    if (doSplitCharSwallow) return text.split(splitPattern);
    Iterable<int> splitIndexes =
        splitPattern.allMatches(text).map((e) => e.start);
    splitIndexes = splitIndexes.toList()..remove(0);
    return splitStringByIndexes(text, splitIndexes);
  }
}

Map<String, Case> caseStyles = {
  'plain': Case.stitched(' '),
  'snake': Case.stitched('_'), // wassup_bruh
  'caterpillar': Case.stitched('-'), // wassup-bruh
  'path': Case.stitched('/'), // wassup/bruh
  'dot': Case.stitched('.', manualSplitPattern: RegExp(r'\.')), // wassup.bruh
  'sentence':
      Case.stitched(' ', doInvertFirstCharCapitalization: true), // Wassup bruh
  'constant': Case.stitched('_', doCapitalize: true), // WASSUP_BRUH
  'train': Case.stitched('-', doCapitalize: true), // WASSUP-BRUH
  'title':
      Case.stitched(' ', doCapitalizeFirstCharOfWords: true), // Wassup Bruh
  'header':
      Case.stitched('-', doCapitalizeFirstCharOfWords: true), // Wassup-Bruh
  'pascal': Case(RegExp('[A-Z]'), false, (String total, String word) {
    word = makeFirstCharCaps(word.toLowerCase());
    if (total.isEmpty) return word;
    return total + word;
  }), // WassupBruh
  'camel': Case(RegExp('[A-Z]'), false, (String total, String word) {
    word = word.toLowerCase();
    if (total.isEmpty) return word;
    word = makeFirstCharCaps(word);
    return total + word;
  }), // wassupBruh
};

// Case snakeCase = Case.stitched('_'); // wassup_bruh
// Case caterpillarCase = Case.stitched('-'); // wassup-bruh
// Case pathCase = Case.stitched('/'); // wassup/bruh
// Case dotCase =
//     Case.stitched('.', manualSplitPattern: RegExp(r'\.')); // wassup.bruh
// Case sentenceCase =
//     Case.stitched(' ', doInvertFirstCharCapitalization: true); // Wassup bruh
// Case constantCase = Case.stitched('_', doCapitalize: true); // WASSUP_BRUH
// Case trainCase = Case.stitched('-', doCapitalize: true); // WASSUP-BRUH
// Case titleCase =
//     Case.stitched(' ', doCapitalizeFirstCharOfWords: true); // Wassup Bruh
// Case headerCase =
//     Case.stitched('-', doCapitalizeFirstCharOfWords: true); // Wassup-Bruh
// Case pascalCase = Case(RegExp('[A-Z]'), false, (String total, String word) {
//   word = makeFirstCharCaps(word.toLowerCase());
//   if (total.isEmpty) return word;
//   return total + word;
// }); // WassupBruh
// Case camelCase = Case(RegExp('[A-Z]'), false, (String total, String word) {
//   word = word.toLowerCase();
//   if (total.isEmpty) return word;
//   word = makeFirstCharCaps(word);
//   return total + word;
// }); // wassupBruh

int main(List<String> args) {
  // Parsing
  var argParser = ArgParser()
    ..addOption('from', abbr: 'f')
    ..addOption('to', abbr: 't');
  List<String> wordsInArgs = [];
  String? stdinLineBuffer;
  ArgResults parsedArguments = argParser.parse(args);
  // Error handling
  if (caseStyles[parsedArguments['from']] == null ||
      caseStyles[parsedArguments['to']] == null) {
    stderr.write('I dont know that case style, fuck off\n');
    return 1;
  }
  Case fromCase = caseStyles[parsedArguments['from']]!;
  Case toCase = caseStyles[parsedArguments['to']]!;

  if (parsedArguments.rest.isEmpty) {
    while (true) {
      stdinLineBuffer = stdin.readLineSync();
      if (stdinLineBuffer == null) break;
      print(toCase.encode(fromCase.decode(stdinLineBuffer)));
    }
  } else {
    parsedArguments.rest.forEach((rawText) {
      wordsInArgs.addAll(fromCase.decode(rawText));
    });
    stdout.write(toCase.encode(wordsInArgs) + '\n');
  }
  return 0;
}

List<String> testCase(Case c, List<String> words) {
  String encoded = c.encode(words);
  print(encoded);
  words = c.decode(encoded);
  // print('DECODED: ' + words.toString());
  return words;
}


// snake_case or c_case
// PascalCase or CapitalCamelCase
// camelCase
// MACRO_CASE or UPPER_CASE
// kebab-case. Also called caterpillar-case, dash-case, hyphen-case, lisp-case, spinal-case and css-case
// COBOL-CASE or TRAIN-CASE
// Sentence case
// Title Case
// path/case
// dot.case
// Header-Case
